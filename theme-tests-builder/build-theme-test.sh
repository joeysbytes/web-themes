#!/bin/bash
set -e

THEME_TEST_BUILDER_DIR="$(pwd)"
THEME_TEST_FILES_DIR="${THEME_TEST_BUILDER_DIR}/../theme-tests"
THEME_TEST_TEMPLATE_DIR="${THEME_TEST_BUILDER_DIR}/template"
THEME_TESTS_DIR="${THEME_TEST_BUILDER_DIR}/tests"
THEMES_DIR="${THEME_TEST_BUILDER_DIR}/../themes"
NAVIGATION=""

theme_test_dir=""
theme_test_full_dir=""
theme_test_filename=""
theme_test_full_filename=""


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    build_navigation
    process_themes
}


###########################################################################
# Build the navigation links for the top of each page.
###########################################################################
function build_navigation() {
    echo "Building navigation..."
    # Beginning tags
    NAVIGATION="<p id=\"navigation\">"
    NAVIGATION="${NAVIGATION}"$'\n'
    NAVIGATION="${NAVIGATION}    "
    # Navigation links
    for full_filename in ${THEME_TESTS_DIR}/*.html
    do
        nav_title=$(basename "${full_filename}" ".html")
        echo "  ${nav_title}"
        NAVIGATION="${NAVIGATION}<a href=\"./${nav_title}.html\">${nav_title}</a> * "
    done
    # Remove final bullet *
    nav_length=${#NAVIGATION}
    nav_length=$((nav_length-3))
    NAVIGATION="${NAVIGATION:0:nav_length}"
    # Ending tags
    NAVIGATION="${NAVIGATION}"$'\n'
    NAVIGATION="${NAVIGATION}</p>"
    NAVIGATION="${NAVIGATION}"$'\n'
    NAVIGATION="${NAVIGATION}<hr>"
    NAVIGATION="${NAVIGATION}"$'\n'
    NAVIGATION="${NAVIGATION}<br>"
    NAVIGATION="${NAVIGATION}"$'\n'
}


###########################################################################
# Loop through each theme directory.
###########################################################################
function process_themes() {
    for theme_dir in $(find "${THEMES_DIR}" -mindepth 1 -maxdepth 1 -type d)
    do
        theme_test_dir=$(basename "${theme_dir}")
        build_theme_test_files
    done
}


###########################################################################
# Build the theme test files for a specific theme.
###########################################################################
function build_theme_test_files() {
    echo ""
    echo "Building theme test files for: ${theme_test_dir}"
    theme_test_full_dir="${THEME_TEST_FILES_DIR}/${theme_test_dir}"
    setup_theme_test_dir
    for full_filename in ${THEME_TESTS_DIR}/*.html
    do
        theme_test_filename=$(basename "${full_filename}")
        theme_test_full_filename="${theme_test_full_dir}/${theme_test_filename}"
        build_a_theme_test_file
    done
}


# Delete, then create empty theme test directory
function setup_theme_test_dir() {
    cd "${THEME_TEST_FILES_DIR}"
    set +e
    rm -rf "${theme_test_dir}"
    set -e
    mkdir "${theme_test_dir}"
    cd "${THEME_TEST_BUILDER_DIR}"
}


# Build an individual theme test file
function build_a_theme_test_file() {
    echo "  ${theme_test_filename}"
    local nav_title=$(basename "${theme_test_full_filename}" ".html")
    touch "${theme_test_full_filename}"
    append_template_file "html_00.html"
    append_template_file "head_00.html"
    append_head_title "${nav_title}"
    append_stylesheet
    append_template_file "head_99.html"
    append_template_file "body_00.html"
    append_body_title "${nav_title}"
    append_text "${NAVIGATION}"
    append_file "${full_filename}"
    append_template_file "body_99.html"
    append_template_file "html_99.html"
}


# Append the contents of a file from the template directory.
function append_template_file() {
    local template_file="${THEME_TEST_TEMPLATE_DIR}/${1}"
    cat "${template_file}">>"${theme_test_full_filename}"
}


# Append the contents of a file from the given file path.
function append_file() {
    local file_name="${1}"
    cat "${file_name}">>"${theme_test_full_filename}"
}


# Append the text passed in.
function append_text() {
    local template_text="${1}"
    echo "${template_text}">>"${theme_test_full_filename}"
}


# Append the head <title> tag.
function append_head_title() {
    local theme_test_nav="${1}"
    local text="    <title>THEME TEST: ${theme_test_dir} / ${theme_test_nav}</title>"
    append_text "${text}"
}


# Add the link to the stylesheet
function append_stylesheet() {
    local href="../../themes/${theme_test_dir}/css/main.css"
    text="    <link"
    text="${text} id=\"theStylesheet\""
    text="${text} rel=\"stylesheet\""
    text="${text} href=\"${href}\""
    text="${text}>"
    append_text "${text}"
}


# Append the body title information.
function append_body_title() {
    local theme_test_nav="${1}"
    local text="<p>THEME TEST: ${theme_test_dir} / ${theme_test_nav}</p>"
    text="${text}"$'\n'
    text="${text}<br>"
    text="${text}"$'\n'
    append_text "${text}"
}

main
