These fonts are:

* From the [Ultimate Oldschool PC Font Pack](https://int10h.org/oldschool-pc-fonts/)
* Distributed under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)
* [Other "Legal" writings](https://int10h.org/oldschool-pc-fonts/readme/#legal_stuff)
* Used courtesy of "VileR".
