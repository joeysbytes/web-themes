# ANSI Theme

This theme is written to mimic an ANSI terminal, while at the same time supporting the most commonly used HTML elements.  I have tried my best to make this theme pixel perfect, so it appears as close as possible to the spirit of how it would on a text terminal.

## Usage

Copy the whole ANSI folder (or link to it) from your project, and import the color scheme you wish to use.  See Color Schemes below.

## Fonts

The fonts used in this theme are the IBM VGA 9x16 fonts.  These were obtained from the [int10h Ultimate Oldschool PC Fonts](https://int10h.org/oldschool-pc-fonts/).  Both square and aspect-correct fonts are available in the fonts folder.  The 80-column aspect-corrected font is the chosen font for this theme.

*Note:*  See the readme in the fonts folder for more information.

## Color Variables

There are 18 CSS color variables pre-defined for you, which adhere to the ANSI/DOS standard.

| Normal | Bright |
| --- | ---|
| --black | --bright-black |
| --blue | --bright-blue |
| --green | --bright-green |
| --cyan | --bright-cyan |
| --red | --bright-red |
| --magenta | --bright-magenta |
| --brown or --orange | --bright-yellow |
| --yellow | --bright-yellow |
| --white | --bright-white |

*Note:*  In days past, _brown_ (or _orange_) and _yellow_ were the same color code to the computer, but what color actually displayed depended on your graphics capabilities.  Both colors are provided for you to choose from, but to stay true to the ANSI standard, pick one and use that as the color for your whole web page.

*Note:*  There is no bright version of _brown_ (or _orange_).  The ANSI standard only had _bright-yellow_ to compliment this color.

## 256 Color Variables

If you would like a 256-color VGA palette, include the file _color256-vars.css_.  This will give you color variables named _--color0_ to _--color255_.

_Note:_  You can see a 256-color chart [here](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit). 

## Color Schemes

### main.css

White text on a black background.

* This is the main color scheme that the other color schemes derive from.
  * The main colors used are:
    * white
    * black
    * bright-white
    * bright-black
* This theme is almost completely a black and white theme for you to easily override and colorize to your liking.
* Links are colored with _bright-blue_ (similar to a browser coloring links blue), because I felt they did not stick out on a page enough for the user to realize they are links.
* The mark tag uses _yellow_ as a background color.
