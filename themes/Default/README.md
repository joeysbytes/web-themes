# Default Theme

This is an empty theme.  It's only purpose is to test what the browser's default CSS looks like.

# Usage

Do nothing.  You can include the _main.css_ file, but it will not do anything.
